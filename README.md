# ddns-henet

Dynamic DNS updater for Hurricane Electric (HE.net)

## Description

Hurricane Electric supports a DDNS service, to keep certain `A` or `AAAA`
records in sync.

The service documentation is here:

https://dns.he.net/docs.html

## Installation

The script to be executed is `ddns-henet-update`, and there are SystemD files
in `ddns-henet-update@.*`. The paths assume standard locations.

## Usage

To update the domain `$DOMAIN`:

- Add the `A`(IPv4) or `AAAA`(IPv6) record and mark the "Enable entry for
  Dynamic DNS" option.
- Generate a password to update the domain: `$PASSWORD`. The service has a
  "Generate Key" option, which is sufficient.
- Create the `/etc/ddns-henet/$DOMAIN.credentials` file, with these contents:

  ```bash
  #IPv4=yes
  #IPv6=yes
  PASSWORD=$PASSWORD
  ```
  
  By default, both IP versions are updated, but you disable one (or both!) by
  uncommenting the corresponding line and changing the value to `no`.
- Check is all goes well by running:

  ```bash
  systemctl start ddns-henet-update@$DOMAIN
  ```

- If that goes well, enable the timer to run this automatically:

  ```bash
  systemctl enable --now ddns-henet-update@$DOMAIN
  ```

## Alternatives

The only software I found that supports this provider is [godns][godns].

[godns]: https://github.com/TimothyYe/godns/
